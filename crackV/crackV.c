#define _CRT_SECURE_NO_DEPRECATE
#include <stdio.h>
#define KEY_LENGTH 2 // Can be anything from 1 to 13
typedef int bool;
enum { false, true };

int hexstring_to_int(const char *hexstring){
	int number = (int)strtol(hexstring, NULL, 16); 
	return number;
}

void printCiphertext_ASCII(char *path){
	printf("\nCipher ASCII HEX to Printable text:");
	unsigned char ch;
	FILE *archivo;
	archivo = fopen(path, "r");
	printf("\n");
	unsigned char low = 0;
	char hexstring[] = { 0, 0 };
	int i = 0;
	while (fscanf(archivo, "%c", &ch) != EOF){
		if (i % 2 != 0){//Second
			i = 2;
			hexstring[0] = low;
			hexstring[1] = ch;
			printf("%c", hexstring_to_int(hexstring));
		}
		else{//First
			i = 1;
			low = ch;
		}
	}
	fclose(archivo);
}

void cipherPlainText(char *inputfile,char *outputfile){
	unsigned char ch;
	FILE *fpIn, *fpOut;
	int i;
	unsigned char key[KEY_LENGTH] = { 0xA1, 0x2F };
	fpIn = fopen(inputfile, "r");
	fpOut = fopen(outputfile, "w");

	i = 0;
	while (fscanf(fpIn, "%c", &ch) != EOF) {
		if (ch != '\n') {
			fprintf(fpOut, "%02X", ch ^ key[i % KEY_LENGTH]); // ^ is logical XOR    
			i++;
		}
	}
	fclose(fpIn);
	fclose(fpOut);
}

char* fileToArray(char *ctext){
	char ch;
	char ch2;
	char hexstring[]= "AA";
	int numberOfChars = 0;
	FILE * fpCtext = fopen(ctext, "r");

	while (fscanf(fpCtext, "%c", &ch) != EOF){
		numberOfChars++;
	}fclose(fpCtext);

	char *ctext_buffer = (char *)malloc(sizeof(char)*numberOfChars);
	fpCtext = fopen(ctext, "r");
	int count = 0; while (fscanf(fpCtext, "%c", &ch) != EOF){		
		
		hexstring[0] = ch;
		fscanf(fpCtext, "%c", &ch2);			
		hexstring[1] = ch2;
		printf("%s", hexstring);
		//printf("%d(%c%c)",count, ch, ch2);
		ctext_buffer[count] = hexstring_to_int(hexstring);
		
		count++;
	}fclose(fpCtext);

	return ctext_buffer;
}
int sizeOfText(char *text){
	FILE *fpCtext = fopen(text, "r"); const char ch;
	int count = 0; while (fscanf(fpCtext, "%c", &ch) != EOF){
		count++;
	}fclose(fpCtext);
	return count;
}

void decryptCtextWithKey(char *ctext,int size_of_ctext,unsigned char *key,int size_of_key){
	printf("\nKey: "); for (int i = 0; i < size_of_key; i++){
		printf("%02X ", key[i]);
	}
	printf("\nDecryption in HEX:\n");
	for (int i = 0; i < size_of_ctext; i++){
		printf("%02X ", ctext[i] ^ key[i % size_of_key]); // ^ is logical XOR 
	}
	printf("\nDecryption in ASCII:\n");
	for (int i = 0; i < size_of_ctext; i++){
		printf("%c", ctext[i] ^ key[i % size_of_key]); // ^ is logical XOR 
	}
}

void crack(char *ctext){
	char *myArray = fileToArray(ctext);
	int size = sizeOfText(ctext);
	char key[2] = { 0xA1, 0x2F };
	decryptCtextWithKey(myArray, size, key, 2);
	free(myArray);
}

void printPlainText(char *path){
	unsigned char ch;
	//printf("\nPlain Text in ASCII:\n");
	FILE *archivo;
	archivo = fopen(path, "r");
	while (fscanf(archivo, "%c", &ch) != EOF){
		printf("%c", ch);
	}
	
}

void printPlainText02X(char *path){
	unsigned char ch;
	FILE *archivo;
	archivo = fopen(path, "r");
	while (fscanf(archivo, "%c", &ch) != EOF){
		printf("%02X ", ch);
	}
}

main(){
	printf("Size of char: %02X",sizeof(char));
	printf("\nPT= "); printPlainText("ptext.txt");
	printf("\nPT_0x= "); printPlainText02X("ptext.txt");

	cipherPlainText("ptext.txt","ctext.txt");
	printf("\nCT="); printPlainText("ctext.txt");

	printf("\nCrack:\n"); crack("ctext.txt");
	printf("\n");
	return;
} 